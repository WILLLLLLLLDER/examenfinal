<?php
    require_once('auth.php');
?>
<?php
require_once('connection/config.php');
    $link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
    if(!$link) {
        die('Fallo la conexion con el servidor' . mysql_error());
    }
    
    $db = mysql_select_db(DB_DATABASE);
    if(!$db) {
        die("no se pudo seleccionar la base de datos");
    }

$result=mysql_query("SELECT * FROM categorias")
or die("no se encontraron registros" . mysql_error()); 
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Admin Index</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div>
<div class="container">
 	<div class="btn-group btn-group-lg" role="group">
    <a class="btn btn-primary" href="categorias.php" role="button">CATEGORIAS</a>
    <a class="btn btn-primary" href="peliculas.php" role="button">PELICULAS</a>
	<a class="btn btn-primary" href="reservas.php" role="button">RESERVAS</a>
	<a class="btn btn-primary" href="opciones.php" role="button">OPCIONES</a>
	<a class="btn btn-danger" href="cerrarsesion.php" role="button">CERRAR SESION</a>
</div><BR><BR>
<div class="container">
<form class="form-inline" name="categoriaForm" id="categoriaForm" action="categorias-exec.php" method="post" onsubmit="return categoriasValidate(this)">
  <div class="form-group">
    <label for="first_name" class="sr-only">nombre de la categoria</label>
    <input type="text" class="form-control" name="name" placeholder="Ingrese la categoria">
  </div>
  <button type="submit" name="Submit" class="btn btn-success">AGREGAR</button>
</form>
</div><BR>
<table class="table table-hover">
<tr>
<th>Nombre Categoria </th>
<th>Accion</th>
</tr>
<?php
while ($row=mysql_fetch_array($result)){
echo "<tr>";
echo "<td>" . $row['nombreca']."</td>";
echo '<td><a href="eliminar_categoria.php?id=' . $row['categoria_id'] . '">ELIMINAR</a></td>';
echo "</tr>";
}
mysql_free_result($result);
mysql_close($link);
?>
</table>
<hr>
</div>
</body>
</html>