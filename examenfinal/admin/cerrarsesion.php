<?php

	session_start();

	unset($_SESSION['SESS_ADMIN_ID']);
	unset($_SESSION['SESS_ADMIN_NAME']);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>cerrar</title>
</head>
<body>
<h4 align="center" class="err">usted ha cerrado la sesion</h4>
<p align="center"><a href="login.php">Click aqui</a> para iniciar sesion</p>
</body>
</html>
