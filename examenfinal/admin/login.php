<?php
  require_once('connection/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Login</title>
<link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<div class="container">
<form class="form-horizontal" id="loginForm" name="loginForm" method="post" action="ingresar.php" onsubmit="return loginValidate(this)">
  <div class="form-group">
    <label class="col-lg-2 control-label">LOGIN</label>
    <div class="col-lg-5">
    <input name="login" type="text" id="login" class="form-control" placeholder="escribe aqui tu login">
    </div>
  </div>
  <div class="form-group">
    <label class="col-lg-2 control-label">CONTRASEÑA</label>
    <div class="col-lg-5">
    <input name="password" type="password" id="password" class="form-control" placeholder="escribe aqui tu contraseña">
    </div>
  </div>
   <div class="col-lg-5">
    <button type="submit" name="Submit" class="btn btn-success">iniciar</button>
   </div>
</form>
</div>
</body>
</html>
