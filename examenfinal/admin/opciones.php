<?php
    require_once('auth.php');
?>
<?php
require_once('connection/config.php');

    $link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
    if(!$link) {
        die('Fallo la conexion con el servidor' . mysql_error());
    }
    
    $db = mysql_select_db(DB_DATABASE);
    if(!$db) {
        die("no se pudo seleccionar la base de datos");
    }
    
$categorias=mysql_query("SELECT * FROM categorias")
or die("no se encontraron registros" . mysql_error()); 

$cantidades=mysql_query("SELECT * FROM cantidades")
or die("algo esta mal" . mysql_error()); 

$monedas=mysql_query("SELECT * FROM monedas")
or die("algo esta mal" . mysql_error()); 

$monedas_1=mysql_query("SELECT * FROM monedas")
or die("algo esta mal" . mysql_error()); 

$preguntas=mysql_query("SELECT * FROM preguntas")
or die("algo esta mal" . mysql_error());
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>opciones</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
 	<div class="btn-group btn-group-lg" role="group">
    <a class="btn btn-primary" href="categorias.php" role="button">CATEGORIAS</a>
    <a class="btn btn-primary" href="peliculas.php" role="button">PELICULAS</a>
	<a class="btn btn-primary" href="reservas.php" role="button">RESERVAS</a>
	<a class="btn btn-primary" href="opciones.php" role="button">OPCIONES</a>
	<a class="btn btn-danger" href="cerrarsesion.php" role="button">CERRAR SESION</a>
</div><BR><BR>
<CAPTION><h3>OPCIONES DE CATEGORIA</h3></CAPTION>
<table class="table table-hover">
<tr>
<form name="categoriaForm" id="categoriaForm" action="categorias-exec.php" method="post" onsubmit="return categoriasValidate(this)">
<td>
  <table class="table table-hover">
    <tr>
        <td><input type="text" name="name" class="form-control" placeholder="Ingrese la categoria"/></td>
        <td><button type="submit" name="Insertar" class="btn btn-success">AGREGAR</button></td>
    </tr>
  </table> 
</td>
</form>
<td>
<form name="categoriaForm" id="categoriaForm" action="eliminar_categoria.php" method="post" onsubmit="return categoriasValidate(this)">
  <table class="table table-hover">
    <tr>
        <td><select name="categoria" id="categoria" class="form-control">
        <option value="select">Seleccionar categoria
        <?php 
        while ($row=mysql_fetch_array($categorias)){
        echo "<option value=$row[categoria_id]>$row[nombreca]"; 
        }
        ?>
        </select></td>
        <td><button type="submit" name="eliminar" class="btn btn-success">ELIMINAR</button></td>
    </tr>
  </table>
</form>
</td>
</tr>
</table>
<hr>
<CAPTION><h3>OPCIONES DE CANTIDADES</h3></CAPTION>
<table class="table table-hover">
<tr>
<form name="cantidadForm" id="cantidadForm" action="cantidades-exec.php" method="post" onsubmit="return cantidadesValidate(this)">
<td>
  <table class="table table-hover">
    <tr>
        <td><input type="text" name="name" class="form-control" placeholder="Ingrese una cantidad" /></td>
        <td><button type="submit" name="Insertar" class="btn btn-success">AGREGAR</button></td>
    </tr>
  </table>
</td>
</form>
<td>
<form name="cantidadForm" id="cantidadForm" action="eliminar_cantidad.php" method="post" onsubmit="return cantidadesValidate(this)">
  <table class="table table-hover">
    <tr>
        <td>Cantidad</td>
        <td><select name="cantidad" id="cantidad" class="form-control">
        <option value="select">Selecciona la cantidad
        <?php 
        while ($row=mysql_fetch_array($cantidades)){
        echo "<option value=$row[cantidad_id]>$row[valor_cantidad]"; 
        }
        ?>
        </select></td>
        <td><button type="submit" name="eliminar" class="btn btn-success">ELIMINAR</button></td>
    </tr>
  </table>
</form>
</td>
</tr>
</table>
<hr>
<CAPTION><h3>OPCIONES DE LAS MONEDAS</h3></CAPTION>
<table class="table table-hover">
<tr>
<td>
<form name="monedasForm" id="monedasForm" action="monedas-exec.php" method="post" onsubmit="return monedasValidate(this)">
  <table class="table table-hover">
    <tr>
    <td><input type="text" name="name" class="form-control" placeholder="Ingrese simbolo" /></td>
    <td><button type="submit" name="Insertar" class="btn btn-success">AGREGAR</button></td>
    </tr>
  </table>
</form>
</td>
<td>
<form name="monedasForm" id="monedasForm" action="eliminar_moneda.php" method="post" onsubmit="return monedasValidate(this)">
  <table class="table table-hover">
    <tr>
        <td><select name="monedas" id="monedas" class="form-control">
        <option value="select">Seleccionar moneda
        <?php 
        while ($row=mysql_fetch_array($monedas)){
        echo "<option value=$row[monedas_id]>$row[simbolo]"; 
        }
        ?>
        </select></td>
        <td><button type="submit" name="eliminar" class="btn btn-success">ELIMINAR</button></td>
    </tr>
  </table>
</form>
</td>
<td>
<form name="monedasForm" id="monedasForm" action="activar_moneda.php" method="post" onsubmit="return monedasValidate(this)">
  <table class="table table-hover">
    <tr>
        <td><select name="monedas" id="monedas" class="form-control">
        <option value="select">seleccionar moneda
        <?php 
        while ($row=mysql_fetch_array($monedas_1)){
        echo "<option value=$row[monedas_id]>$row[simbolo]"; 
        }
        ?>
        </select></td>
        <td><button type="submit" name="actualizar" class="btn btn-success">ACTIVAR</button></td>
    </tr>
  </table>
</form>
</td>
</tr>
</table>
<CAPTION><h3>GESTIONAR PREGUNTAS</h3></CAPTION>
<table class="table table-hover">
<tr>
<form name="preguntasForm" id="preguntasForm" action="preguntas-exec.php" method="post" onsubmit="return preguntasValidate(this)">
<td>
  <table class="table table-hover">
    <tr>
    <td><input type="text" name="name" class="form-control" placeholder="Ingrese una pregunta" /></td>
    <td><button type="submit" name="Insertar" class="btn btn-success">AGREGAR</button></td>
    </tr>
  </table>
</td>
</form>
<td>
<form name="preguntasForm" id="preguntasForm" action="eliminar_pregunta.php" method="post" onsubmit="return preguntasValidate(this)">
  <table class="table table-hover">
    <tr>
        <td><select name="preguntas" id="preguntas" class="form-control">
        <option value="select">seleccionar pregunta
        <?php 

        while ($row=mysql_fetch_array($preguntas)){
        echo "<option value=$row[preguntas_id]>$row[pregunta]"; 
        }
        ?>
        </select></td>
        <td><button type="submit" name="eliminar" class="btn btn-success">ELIMINAR</button></td>
    </tr>
  </table>
</form>
</td>
</tr>
</table>
<hr>
</div>
</div>
</body>
</html>
