<?php
	session_start();
	unset($_SESSION['SESS_MEMBER_ID']);
	unset($_SESSION['SESS_FIRST_NAME']);
	unset($_SESSION['SESS_LAST_NAME']);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>cerrar sesion</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
 	<div class="btn-group btn-group-lg" role="group">
    <a class="btn btn-primary" href="index.php">INICIO</a>
	<a class="btn btn-primary" href="peliculas.php">PELICULAS</a>
	<a class="btn btn-primary" href="miembros-index.php">MI CUENTA</a>
	<a class="btn btn-danger" href="contactos.php">CONTACTANOS</a>
</div>
<h1>Desconectado </h1>
<div class="error">Has sido desconectado.</div>
<p><a href="index.php">Haga click aqui</a> para iniciar sesion</p>
</body>
</html>
