<?php
    require_once('auth.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>facturacion alternativa</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
 	<div class="btn-group btn-group-lg" role="group">
    <a class="btn btn-primary" href="index.php">INICIO</a>
	<a class="btn btn-primary" href="peliculas.php">PELICULAS</a>
	<a class="btn btn-primary" href="miembros-index.php">MI CUENTA</a>
	<a class="btn btn-danger" href="contactos.php">CONTACTANOS</a>
</div>
<div>
<h1>Datos de facturacion</h1>
<hr>
<p>Usted no tiene datos de facturacion en su cuenta. Agregue sus datos aqui</p>
  <div>
<form id="facturacionForm" name="facturacionForm" method="post" action="facturacion-exec.php?id=<?php echo $_SESSION['SESS_MEMBER_ID'];?>" onsubmit="return facturacionValidate(this)">
  <table class="table table-hover">
  <tr>
  <th>DATOS DE FACTURACION</th>
	</tr>
    <tr>
      <th>  Direccion </th>
      <td><input name="sdireccion" type="text" class="form-control" placeholder="direccion" id="sdireccion" /></td>
    </tr>
	<tr>
      <th>Nro targeta</th>
      <td><input name="targeta" type="text" class="form-control" placeholder="numero de targeta" id="targeta" /></td>
    </tr>
    <tr>
      <th>Ciudad </th>
      <td><input name="ciudad" type="text" class="form-control" placeholder="ciudad" id="ciudad" /></td>
    </tr>
    <tr>
      <th>Celular</th>
      <td <input name="mnumero" type="text" class="form-control" placeholder="celular" id="mnumero" /></td>
    </tr>
    <tr>
      <th>Telefono</th>
      <td><input name="lnumero" type="text" class="form-control" placeholder="telefono" id="lnumero" /></td>
    </tr>
    <tr>
      <td><button type="submit" name="Submit" class="btn btn-success">REGISTRAR</button></td>
    </tr>
</table>
</form>
</div>
</div>
</body>
</html>
