<?php
	require_once('auth.php');
?>
<?php
require_once('connection/config.php');

    $link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
    if(!$link) {
        die('Fallo la conexion con el servidor' . mysql_error());
    }

    $db = mysql_select_db(DB_DATABASE);
    if(!$db) {
        die("no se pudo seleccionar la base de datos");
    }

$miembrosId=$_SESSION['SESS_MEMBER_ID'];
?>
<?php

    $flag_0 = 0;
    $items=mysql_query("SELECT * FROM detalles_carrito WHERE miembros_id='$miembrosId' AND bandera='$flag_0'")
    or die("al esta mal" . mysql_error()); 
    $num_items = mysql_num_rows($items);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>perfil</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
 	<div class="btn-group btn-group-lg" role="group">
    <a class="btn btn-primary" href="index.php">INICIO</a>
	<a class="btn btn-primary" href="peliculas.php">PELICULAS</a>
	<a class="btn btn-primary" href="miembros-index.php">MI CUENTA</a>
	<a class="btn btn-danger" href="contactos.php">CONTACTANOS</a>
</div>
<div><a href="miembros-index.php">Inicio</a> | <a href="carrito.php">Carrito de entradas[<?php echo $num_items;?>]</a> | <a href="cerrar_sesion.php">Cerrar sesion</a></div>
<h1>Mi perfil</h1>
<p>Aqui puede cambiar su password y tambien agregar datos de facturacion.
<hr>
<table class="table table-hover">
<tr>
<form id="updateForm" name="updateForm" method="post" action="modificar-exec.php?id=<?php echo $_SESSION['SESS_MEMBER_ID'];?>" onsubmit="return updateValidate(this)">
<td>  
  <table class="table table-hover">
	<tr>
  <th>CAMBIAR CONTRACEÑA</th>
	</tr>
    <tr>
      <th>Contraceña anterior</th>
      <td><input name="opassword" type="password" class="form-control" placeholder="Ingrese su contraceña anterior" id="opassword" /></td>
    </tr>
    <tr>
      <th>Nueva contraceña </th>
      <td><input name="npassword" type="password" class="form-control" placeholder="Ingrese su nueva contraceña" id="npassword" /></td>
    </tr>
    <tr>
      <th>Confirmar contraceña </th>
      <td><input name="cpassword" type="password" class="form-control" placeholder="confirmar contraceña nueva" id="cpassword" /></td>
    </tr>
    <tr>
      <td><button type="submit" name="Submit" class="btn btn-success">MODIFICAR</button></td>
    </tr>
  </table>
</td>
</form>
<td>
<form id="facturacionForm" name="facturacionForm" method="post" action="facturacion-exec.php?id=<?php echo $_SESSION['SESS_MEMBER_ID'];?>" onsubmit="return facturacionValidate(this)">
   <table class="table table-hover">
	<tr>
  <th>DATOS DE FACTURACION</th>
	</tr>
    <tr>
      <th>  Direccion </th>
      <td><input name="sdireccion" type="text" class="form-control" placeholder="direccion" id="sdireccion" /></td>
    </tr>
	<tr>
      <th>Nro targeta</th>
      <td><input name="targeta" type="text" class="form-control" placeholder="numero de targeta" id="targeta" /></td>
    </tr>
    <tr>
      <th>Ciudad </th>
      <td><input name="ciudad" type="text" class="form-control" placeholder="ciudad" id="ciudad" /></td>
    </tr>
    <tr>
      <th>Celular</th>
      <td <input name="mnumero" type="text" class="form-control" placeholder="celular" id="mnumero" /></td>
    </tr>
    <tr>
      <th>Telefono</th>
      <td><input name="lnumero" type="text" class="form-control" placeholder="telefono" id="lnumero" /></td>
    </tr>
    <tr>
      <td><button type="submit" name="Submit" class="btn btn-success">REGISTRAR</button></td>
    </tr>
  </table>
</td>
</form>
</tr>
</table>
</div>
</div>
</body>
</html>